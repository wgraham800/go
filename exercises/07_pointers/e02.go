package main

import "fmt"

type person struct {
	first string
	last  string
	age   int
}

func changeMe(p *person) {
	p.first = "changed"
}

func notChangeMe(p person) {
	p.first = "changed"
}

func main() {
	p := person{
		first: "Cosmo",
		last:  "Kramer",
		age:   34,
	}
	fmt.Println("Original:\t", p)
	notChangeMe(p)
	fmt.Println("Unchanged:\t", p)
	changeMe(&p)
	fmt.Println("Changed!\t", p)
}
