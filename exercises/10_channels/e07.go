package main

import (
	"fmt"
  "sync"
)

func main() {
  c := make(chan int)
  var wg sync.WaitGroup

  wg.Add(10)

  // launch 10 goroutines
  for i := 0; i < 10; i++ {

    // each routine adds 10 numbers to a channel
    go func(){
      for j := 0; j < 10; j++ {
        c <- j
      }
      wg.Done()
    }()
  }

  // Pull the numbers off the channel
  go func(){
    for v := range c {
      fmt.Println("Pulling num off channel:", v)
    }
  }()
  
  wg.Wait()
  close(c)

  fmt.Println("About to exit")
}
