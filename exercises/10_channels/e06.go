package main

import (
	"fmt"
)

func main() {
  c := make(chan int)

  // put 100 numbers on a channel
  go func(){
    for i := 0; i < 100; i++ {
      c <- i
    }
    close(c)
  }()

  // pull 100 numbers off the channel and print them
  for v := range c {
    fmt.Println("Pulling num off channel", v)
  }

  fmt.Println("About to exit")
}
