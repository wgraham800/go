// Create your own type “person” which will have an underlying type of “struct” so that it can store
// the following data:
// ● first name
// ● last name
// ● favorite ice cream flavors
// Create two VALUES of TYPE person. Print out the values, ranging over the elements in the slice
// which stores the favorite flavors.

package main

import (
	"fmt"
)

func main() {
  type person struct {
    fname string
    lname string
    flavors []string
  }

  charlie := person{
    fname: "Charlie",
    lname: "Day",
    flavors: []string{"Chocolate", "Cheese"},
  }

  mac := person{
    fname: "Mac",
    lname: "Dunno",
    flavors: []string{"Superman", "Rainbow"},
  }

  fmt.Println(charlie)
  for _, v := range charlie.flavors {
    fmt.Println(v)
  }
  fmt.Println(mac)
  for _, v := range mac.flavors {
    fmt.Println(v)
  }
}
