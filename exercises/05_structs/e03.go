// ● Create a new type: vehicle.
// ○ The underlying type is a struct.
// ○ The fields:
// ■ doors
// ■ color
// ● Create two new types: truck & sedan.
// ○ The underlying type of each of these new types is a struct.
// ○ Embed the “vehicle” type in both truck & sedan.
// ○ Give truck the field “fourWheel” which will be set to bool.
// ○ Give sedan the field “luxury” which will be set to bool. solution
// ● Using the vehicle, truck, and sedan structs:
// ○ using a composite literal, create a value of type truck and assign values to the
// fields;
// ○ using a composite literal, create a value of type sedan and assign values to the
// fields.
// ● Print out each of these values.
// ● Print out a single field from each of these values.
package main

import (
	"fmt"
)

func main() {
  type vehicle struct {
    doors int
    color string
    electric bool
  }
  type truck struct {
    vehicle
    fourWheel bool
  }
  type sedan struct {
    vehicle
    luxury bool
  }

  rivian := truck{
    vehicle: vehicle {
      doors: 4,
      color: "light blue",
      electric: true,
    },
    fourWheel: true,
  }
  tesla := sedan{
    vehicle: vehicle {
      doors: 4,
      color: "black",
      electric: true,
    },
    luxury: true,
  }

  fmt.Println(rivian)
  fmt.Println(tesla)
  fmt.Println(rivian.fourWheel)
  fmt.Println(tesla.color)
}
