// Create and use an anonymous struct.

package main

import "fmt"

func main() {
  tesla := struct {
    doors int
    color string
    electric bool
  }{
    doors: 4,
    color: "light blue",
    electric: true,
  }

  fmt.Println(tesla)
}
