// Take the code from the previous exercise, then store the values of type person in a map with the
// key of last name. Access each value in the map. Print out the values, ranging over the slice

package main

import (
	"fmt"
)

func main() {
  type person struct {
    fname string
    lname string
    flavors []string
  }

  charlie := person{
    fname: "Charlie",
    lname: "Day",
    flavors: []string{"Chocolate", "Cheese"},
  }

  mac := person{
    fname: "Mac",
    lname: "Dunno",
    flavors: []string{"Superman", "Rainbow"},
  }

  persons := map[string]person{
    charlie.lname: charlie,
    mac.lname: mac,
  }

  for k, v := range persons {
    fmt.Println("Key of :", k)
    fmt.Println(v.fname)
    fmt.Println(v.lname)
    for _, flavor := range v.flavors {
      fmt.Println(flavor)
    }
  }
}
