package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
)

type person struct {
	First   string
	Last    string
	Sayings []string
}

func main() {
	p1 := person{
		First:   "James",
		Last:    "Bond",
		Sayings: []string{"Shaken, not stirred", "Any last wishes?", "Never say never"},
	}

	bs, err := json.Marshal(p1)

	// Forcing an error
	err = errors.New("Try this on for size")
	if err != nil {
		log.Panicln("JSON did not marshal: ", err)
		// log.Fatalln("JSON did not marshal: ", err)
		// log.Println("JSON did not marshal: ", err)
	}

	fmt.Println(string(bs))

}
