package main

import (
	"fmt"
)

type customError struct {
	info string
}

func (ce customError) Error() string {
	// return ce.err
	return fmt.Sprintf("Here is the error... %v", ce.info)
}

func main() {
	c1 := customError{
		info: "Error!",
	}
	foo(c1)
}

func foo(e error) {
	fmt.Println(e)
	// fmt.Println(e.(customError).info)
	// fmt.Printf("This is assertion! %v", e.(customError).info)
}
