package main

import (
	"fmt"
)

func main() {
  year, i := 1994, 0

  for {
    fmt.Println("I was alive in", year+i)
    i++
    if i > 28 {
      break;
    }
  }
}
