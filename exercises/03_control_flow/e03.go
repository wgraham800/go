// Create a for loop using this syntax for condition { }
// Have it print out the years you have been alive.
package main

import (
	"fmt"
)

func main() {
  i := 0
  for i < 29 {
    fmt.Println("I was alive in", 1994+i)
    i++
  }
}
