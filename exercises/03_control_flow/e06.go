package main

import (
	"fmt"
)

func main() {
  if i := 0; i > 0 {
    fmt.Println("False")
  } else if i < 0 {
    fmt.Println("False")
  } else {
    fmt.Println("True")
  }
}
