package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Println("Arch is", runtime.GOARCH)
	fmt.Println("OS is", runtime.GOOS)
}
