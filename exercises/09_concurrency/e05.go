package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

var wg sync.WaitGroup
var counter int64

func main() {

	iter := 20
	wg.Add(iter)

	for i := 0; i < iter; i++ {
		go inc()
	}
	wg.Wait()
	fmt.Println("Counter after goroutines is:", counter)
	fmt.Printf("Counter type is %T", counter)
}

func inc() {
	// atomic.LoadInt64(&counter)
	atomic.AddInt64(&counter, 1)
	fmt.Println("Counter inside goroutine is:", atomic.LoadInt64(&counter))
	wg.Done()
}
