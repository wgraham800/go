package main

import (
	"fmt"
)

type person struct {
	name string
}

type human interface {
	speak()
}

func (p *person) speak() {
	fmt.Println("My name is", p.name)
}

func saySomething(h human) {
	h.speak()
}

func main() {
	eminem := person{
		name: "Slim Shady",
	}
	saySomething(&eminem)

	// This does not work
	// saySomething(&eminem)
	eminem.speak()
}
