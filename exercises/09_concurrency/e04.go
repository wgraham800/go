package main

import (
	"fmt"
	"sync"
)

var wg sync.WaitGroup
var mu sync.Mutex
var counter int

func main() {

	iter := 20
	wg.Add(iter)

	for i := 0; i < iter; i++ {
		go inc()
	}
	wg.Wait()
	fmt.Println("Counter after goroutines is:", counter)
}

func inc() {
	mu.Lock()
	tmp := counter
	counter = tmp + 1
	fmt.Println("Counter inside goroutine is:", counter)
	mu.Unlock()
	wg.Done()
}
