package main

import (
	"fmt"
	"runtime"
	"sync"
)

var wg sync.WaitGroup
var counter int

func main() {

	iter := 20
	wg.Add(iter)

	for i := 0; i < iter; i++ {
		go inc()
	}
	wg.Wait()
	fmt.Println("Counter after goroutines is:", counter)
}

func inc() {
	tmp := counter
	runtime.Gosched()
	counter = tmp + 1
	fmt.Println("Counter inside goroutine is:", counter)
	wg.Done()
}
