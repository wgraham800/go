//  create a type rectangle
// ● create a type CIRCLE
// ● attach a method to each that calculates AREA and returns it
// ○ circle area= π r 2
// ○ rectangle area = L * W
// ● create a type SHAPE that defines an interface as anything that has the AREA method
// ● create a func INFO which takes type shape and then prints the area

package main

import (
	"fmt"
	"math"
)

type rectangle struct {
	length float64
	width  float64
}

type circle struct {
	radius float64
}

type shape interface {
	area() float64
}

func (c circle) area() float64 {
	return math.Pow((c.radius * math.Pi), 2)
}

func (s rectangle) area() float64 {
	return s.length * s.width
}

func main() {
	c := circle{
		radius: 5.23,
	}
	s := rectangle{
		length: 5.2,
		width:  3.4,
	}

	// Turns out there's no easy / great way to round numbers in Go...
	// Here I'm specifically creating a format string of 'print 3 decimal places'
	fmt.Printf("Area of circle: %.3f\n", info(c))
	fmt.Printf("Area of square: %v\n", info(s))
}

func info(s shape) float64 {
	return s.area()
}
