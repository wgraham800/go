package main

import "fmt"

func main() {
	x := 5
	fmt.Println("X equals", x)

	{
		x := 10
		fmt.Println("X equals", x)
	}

	fmt.Println("X equals", x)
}
