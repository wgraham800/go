package main

import "fmt"

func main() {
	f := func() {
		fmt.Println("This func is assigned to f and passed into another func.")
	}
	foo(f)
}

func foo(f func()) {
	f()
}
