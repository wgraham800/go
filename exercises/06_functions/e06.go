package main

import "fmt"

func main() {
	x := func() {
		fmt.Println("This func is assigned to 'x'.")
	}
	x()
}
