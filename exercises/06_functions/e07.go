package main

import "fmt"

func main() {
	x := foo()
	x()
}

func foo() func() {
	return func() {
		fmt.Println("This func is returned from another func.")
	}
}
