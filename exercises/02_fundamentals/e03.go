package main

import "fmt"

const (
	a        = 42
	b        = "No thank you"
	c int    = 63
	d string = "Yes Please"
)

func main() {
	fmt.Println(a, b)
	fmt.Println(c, d)
}
