package main

import "fmt"

func main() {
	s := `This is a string
	that is still a 
	string`

	bs := []byte(s)

	fmt.Println(s)
	fmt.Println(bs)
}
