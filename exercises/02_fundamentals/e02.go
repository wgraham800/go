package main

import "fmt"

func main() {
	n := 256

	g := (n == n)
	h := (n <= n)
	i := (n >= n)
	j := (n != n)
	k := (n < n)
	l := (n > n)
	fmt.Print(g, h, i, j, k, l)
}
