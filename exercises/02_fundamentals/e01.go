package main

import "fmt"

func main() {
	n := 256

	fmt.Printf("%d\n", n)
	fmt.Printf("%#b\n", n)
	fmt.Printf("%#x\n", n)
}
