package main

import "fmt"

const (
	_  = iota
	y1 = 2022 + iota
	y2 = 2022 + iota
	y3 = 2022 + iota
	y4 = 2022 + iota
)

func main() {
	fmt.Println(y1)
	fmt.Println(y2)
	fmt.Println(y3)
	fmt.Println(y4)
}
