package main

import (
	"fmt"
)

func main() {
  x := map[string][]string{
    `bond_james`: {`Shaken, not stirred`, `Martinis`, `Women`},
    `moneypenny_miss`: {`James Bond`, `Literature`, `Computer Science`},
    `no_dr`: {`Being evil`, `Ice cream`, `Sunsets`},
  }

  for k, v := range x {
    fmt.Printf("The record %v contains\n", k)

    for i, v2 := range v {
      fmt.Println(i, v2)
    }
  }
}
