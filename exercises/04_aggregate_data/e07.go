package main

import (
	"fmt"
)

func main() {
  x := [][]string{{"Dee", "Charlie", "Dennis", "Mac"},{"Frank", "The Waitress", "Cricket"}}

  for _, a := range x {
    for _, b := range a {
      fmt.Println(b)
    }
  }
}
