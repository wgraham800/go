// Using the code from the previous example, use SLICING to create the following new slices
// which are then printed:
// ● [42 43 44 45 46]
// ● [47 48 49 50 51]
// ● [44 45 46 47 48]
// ● [43 44 45 46 47]

package main

import (
	"fmt"
)

func main() {
  int_array := []int{41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51}

  a :=  int_array[1:6]
  b :=  int_array[6:]
  c :=  int_array[3:8]
  d :=  int_array[2:7]
  fmt.Println(a)
  fmt.Println(b)
  fmt.Println(c)
  fmt.Println(d)
}
