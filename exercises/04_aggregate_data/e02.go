// Using a COMPOSITE LITERAL:
// ● create a SLICE of TYPE int
// ● assign VALUES
// ● Range over the slice and print the values out.
// ● Using format printing
// ○ print out the TYPE of the slice

package main

import (
	"fmt"
)

func main() {
  int_array := []int{0, 1, 2, 3, 4}

  fmt.Printf("Type is : %T\n", int_array)
  for x := range int_array {
    fmt.Println(x)
  }
}
