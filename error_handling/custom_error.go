package main

import (
	"fmt"
	"log"
)

type MathError struct {
	lat  string
	long string
	err  error
}

func (m MathError) Error() string {
	return fmt.Sprintf("A math error occurred: %v %v %v", m.lat, m.long, m.err)
}
func main() {
	_, err := sqrt(-10)
	if err != nil {
		log.Println(err)
	}
}

func sqrt(f float64) (float64, error) {
	if f < 0 {
		me := fmt.Errorf("Math error: square root of negative number %v", f)
		return 0, MathError{"50.229", "28,82093", me}
	}
	return 42, nil
}
