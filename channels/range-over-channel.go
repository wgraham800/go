package main

import (
	"fmt"
)

func main() {
	c := make(chan int)

	go func() {
		for i := 0; i < 10; i++ {
			c <- i
			fmt.Printf("Adding %v to the channel.\n", i)
		}
		close(c)
	}()

	for i := range c {
		fmt.Printf("Reading value %v from the channel using range.\n", i)
	}

}
