package main

import (
	"fmt"
)

func main() {
	c := make(chan int)

	go send(c)
	recv(c)
}

func recv(c <-chan int) {
	fmt.Println("Reading from chan c:", <-c)
}

func send(c chan<- int) {
	fmt.Println("Sending to chan c:")
	c <- 42
}
