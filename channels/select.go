package main

import (
	"fmt"
)

func main() {
	even := make(chan int)
	odd := make(chan int)
	quit := make(chan bool)

	go send(even, odd, quit)

	recv(even, odd, quit)
}

func recv(even, odd <-chan int, quit <-chan bool) {
	for {
		select {
		case v := <-even:
			fmt.Println("Pulling value from even channel:\t", v)
		case v := <-odd:
			fmt.Println("Pulling value from odd channel:\t\t", v)
		case v, ok := <-quit:
			if !ok {
				fmt.Println("Pulling from quit is okay ?", v)
			} else {
				fmt.Println("Pulling from quit is okay ?", v)
			}
			return
		}
	}
}

func send(even, odd chan<- int, quit chan<- bool) {
	for i := 0; i < 15; i++ {
		if i%2 == 0 {
			even <- i
		} else {
			odd <- i
		}
	}
	close(quit)

	// NOTE - not closing the channel makes this cleaner for some reason
}
